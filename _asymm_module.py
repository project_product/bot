#!/usr/bin/env python3
import time
import gnupg
import rsa
from rsa import key, common, decrypt, encrypt

f = open('./keys/config')
keys = f.read()

pub_key = ''
priv_key = ''

if keys == '1':
    with open('./keys/public_512.pem', mode='rb') as publicfile:
        keydata = publicfile.read()
        pub_key = rsa.PublicKey.load_pkcs1(keydata)

    with open('./keys/private_512.pem', mode='rb') as privatefile:
        keydata = privatefile.read()
        priv_key = rsa.PrivateKey.load_pkcs1(keydata)
elif keys == '2':
    with open('./keys/public_1024.pem', mode='rb') as publicfile:
        keydata = publicfile.read()
        pub_key = rsa.PublicKey.load_pkcs1(keydata)

    with open('./keys/private_1024.pem', mode='rb') as privatefile:
        keydata = privatefile.read()
        priv_key = rsa.PrivateKey.load_pkcs1(keydata)
elif keys == '3':
    with open('./keys/public_2048.pem', mode='rb') as publicfile:
        keydata = publicfile.read()
        pub_key = rsa.PublicKey.load_pkcs1(keydata)

    with open('./keys/private_2048.pem', mode='rb') as privatefile:
        keydata = privatefile.read()
        priv_key = rsa.PrivateKey.load_pkcs1(keydata)
elif keys == '4':
    with open('public.pem', mode='rb') as publicfile:
        keydata = publicfile.read()
        pub_key = rsa.PublicKey.load_pkcs1(keydata)

    with open('private.pem', mode='rb') as privatefile:
        keydata = privatefile.read()
        priv_key = rsa.PrivateKey.load_pkcs1(keydata)


f = open('config')
key = f.read()

gpg = gnupg.GPG(gnupghome='/root/gpghome')

def asymmetric_encrypt(m):
    global pub_key
    # шифруем
    encrypted_message = encrypt(m.encode('utf-8'), pub_key)

    return encrypted_message


def asymmetric_decrypt(m):
    global priv_key
    # расшифруем
    decrypted_message = decrypt(m, priv_key)

    return decrypted_message


def asymmetric_encrypt_file(f):
    t_start = time.time()

    status = gpg.encrypt_file(
         f, recipients=['nepogodin.vlad@icloud.com'],
         output=f'{f.name}.gpg')
    
    t_end = time.time() - t_start 

    return t_end * 100

