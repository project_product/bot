#!/usr/bin/env python3
from Crypto.Cipher import AES # алгоритм шифрования
from Crypto.Hash import SHA256 # Для хеширования данных используем также популярный алгоритм SHA.
from Crypto.Hash import MD5 # Этот алгоритм хеширования будет использован для приведения произвольной строки пароля к 32 битной
from Crypto import Random

import pyAesCrypt
from os import stat, remove
import time 

f = open('config')
key = f.read()

def transform_password(password_str):
    """Transform the password string into 32 bit MD5 hash

    :param password_str: <str> password in plain text;

    :return: <str> Transformed password fixed length

    """

    h = MD5.new()
    h.update(key.encode())
    return h.hexdigest()

def symmetric_encrypt(message, verbose = False):
     """Encripts the message using symmetric AES algorythm.

     :param message: <str> Message for encryption;
     :param key: <object> symmetric key;
     :return: <object> Message encrypted with key

     """

     key_MD5 = transform_password(key) # Приводим произвольный пароль к длине 32 бита

     message_hash = SHA256.new(message.encode())
     message_with_hash = message.encode() + message_hash.hexdigest().encode() #Добавим в конец сообщения его хеш. он понадобится нам

     iv = Random.new().read(AES.block_size)

     cipher = AES.new(key_MD5, AES.MODE_CFB, iv) # Создаем объект с заданными параметрами. AES.MODE_CFB - надежный режим шифрования,

     encrypted_message = iv + cipher.encrypt(message_with_hash) # Включаем случайную последовательность в начало шифруемого сообщении

     if verbose:
         return encrypted_message.hex()
     return encrypted_message

def symmetric_decrypt(encr_message, verbose = False):
    """Decripts the message using private_key and check it's hash

    :param encrypted_message: <object> Encrypted message
    :param key: <object> symmetric key;
    :return: <object> Message decripted with key

    """
    key_MD5 = transform_password(key)

    # Размеры боков нужны, для извлечения их из текста
    bsize = AES.block_size
    dsize = SHA256.digest_size * 2

    iv = Random.new().read(bsize)

    cipher = AES.new(key_MD5, AES.MODE_CFB, iv)

    decrypted_message_with_hesh = cipher.decrypt(encr_message)[bsize:] # Извлекаем из блока случайные символу, которые мы добавляли
    decrypted_message = decrypted_message_with_hesh[:-dsize] # Извлекаем хеш сообщения, который мы присоединяли при шифровании

    digest = SHA256.new(decrypted_message).hexdigest() # хеш расшифрованной части сообщения. Он будет сравниваться с хешем, который

    if digest == decrypted_message_with_hesh[-dsize:].decode() and verbose:
        decrypted_message_str = f"Encrypted hash is {decrypted_message_with_hesh[-dsize:].decode()}\nDecrypted hash is {digest}"
        return decrypted_message_str
    elif digest == decrypted_message_with_hesh[-dsize:].decode(): # Если хеш расшифровааного сообщения и хеш, который мы добавили при ш
        return decrypted_message.decode()
    elif verbose:
        decrypted_message_str = decrypted_message + f"Encryption was not correct: the hash of decripted message doesn't match with encrypted hash\nEncrypted hash is {decrypted_message_with_hesh[-dsize:]}\nDecrypted hash is {digest}"
        return decrypted_message_str
    else:
        return decrypted_message



def symmetric_encrypt_file(f):
    bufferSize = 64 * 1024

    t_start = time.time()

    # encrypt
    with open(f'{f.name}', "rb") as fIn:
        with open(f'{f.name}.aes', "wb") as fOut:
            pyAesCrypt.encryptStream(fIn, fOut, key, bufferSize)

    t_end = time.time() - t_start

    return t_end * 100
