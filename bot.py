#!/usr/bin/env python3
import os
import json
import requests
import _symm_module
from _asymm_module import asymmetric_encrypt, asymmetric_decrypt, asymmetric_encrypt_file
import time
import rsa
from rsa import key, common, decrypt, encrypt
import pgpy
from pgpy.constants import PubKeyAlgorithm, KeyFlags, HashAlgorithm, SymmetricKeyAlgorithm, CompressionAlgorithm
import telebot
from telebot import types
from telebot import util
from telebot.types import Message

# for debug
#from telebot import apihelper

TOKEN = '326441442:A7ERjkrer89jjfuErery844nR'


knownUsers = []

u = {
    'users_id': knownUsers
}

with open('db.json') as f:
    u = json.load(f)

commands_en = {  # command description used in the "help" command
    'start': 'Get used to the bot',
    'help': 'Gives you information about the available commands'
}

commands_ru = {  # command description used in the "help" command
    'start': 'Начать пользоваться ботом',
    'help': 'Дает информацию о доступных командах'
}

hideBoard = types.ReplyKeyboardRemove()  # if sent as reply_markup, will hide the keyboard

# FOR RUSSIA
#apihelper.proxy = {'https': 'socks5://616255549:7DS4YYtU@orbtl.s5.opennetwork.cc:999'}

bot = telebot.TeleBot(TOKEN)


#################
def keyboard_en():
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    btn = types.KeyboardButton('Cipher')

    markup.add(btn)
    return markup

def keyboard_ru():
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)

    btn = types.KeyboardButton('Шифр')

    markup.add(btn)
    return markup
#################


###############  START  ################
@bot.message_handler(commands=['start'])
def command_start(m):
    global u
    chat_id = m.chat.id
    lang = m.from_user.language_code

    if chat_id not in u['users_id']:  # if user hasn't used the "/start" command yet:
        u['users_id'].append(chat_id)

        with open('db.json', mode='w') as f:
            f.write(json.dumps(u))

        if lang == 'ru':
            bot.send_message(chat_id, 'Пожалуйста напиши /start сейчас же')
        elif lang == 'en':
            bot.send_message(chat_id, 'Please type /start now')
    else:
        if lang == 'ru':
            bot.send_message(chat_id, 'Я бот, который умеет зашифровывать информацию, и расшифровывать ее', reply_markup=keyboard_ru())
        elif lang == 'en':
            bot.send_message(chat_id, 'I am bot, that can encrypt information and decrypt it', reply_markup=keyboard_en())


@bot.callback_query_handler(func=lambda message:True)
def ans(message):
    global u
    chatid = message.message.chat.id
    messageid = message.message.message_id
    lang = message.message.from_user.language_code

    key = 'Hi'

    # debug
    print(message.data)

    # 1 to 0
    #################################
    if message.data == "ru_ch_return":
        s = requests.Session()
        s.get('https://api.telegram.org/bot{0}/deletemessage?message_id={1}&chat_id={2}'.format(TOKEN, messageid, chatid))

        bot.send_message(chatid, '''\n\nНапиши /start\n\n''''', parse_mode='HTML', reply_markup=keyboard_ru())
    elif message.data == "en_ch_return":
        s = requests.Session()
        s.get('https://api.telegram.org/bot{0}/deletemessage?message_id={1}&chat_id={2}'.format(TOKEN, messageid, chatid))

        bot.send_message(chatid, '''\n\nType /start\n\n''''', parse_mode='HTML', reply_markup=keyboard_en())
    ###################################

    # my 1 to 0
    ################################
    elif message.data == "t_return":
        s = requests.Session()
        s.get('https://api.telegram.org/bot{0}/deletemessage?message_id={1}&chat_id={2}'.format(TOKEN, messageid, chatid))

        bot.send_message(chatid, '''\n\nType /start\n\n''''', parse_mode='HTML', reply_markup=keyboard_en())
    ################################
    

    # my 2 to 1 (Document)
    ##################################
    elif message.data == "_doc_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect type:\n\n''''', parse_mode='HTML', reply_markup=choice_type(chatid))
    ##################################
    
    # my 2 to 1 (Text)
    ##################################
    elif message.data == "_text_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect type:\n\n''''', parse_mode='HTML', reply_markup=choice_type(chatid))
    ##################################
    
    
    # my 3 to 2 (Document-keys)
    ###################################
    elif message.data == "k_doc_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nTo select an algorithm, just click on the cipher:\n\n''''', parse_mode='HTML', reply_markup=choice_encryption_doc(chatid))
    ###################################
    
    # my 3 to 2 (Document-word)
    ###################################
    elif message.data == "w-key_doc_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nTo select an algorithm, just click on the cipher:\n\n''''', parse_mode='HTML', reply_markup=choice_encryption_doc(chatid))
    ###################################
    
    
    # my 4 to 3 (Document-word)
    ###################################
    elif message.data == "w_doc_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose key:\n\n''', parse_mode='HTML', reply_markup=choice_key_doc(chatid))
    ###################################


    # 2 to 1 (Word)
    ##################################
    elif message.data == "ru_w_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nДля выбора алгоритма нажмите на шифр:\n\n''', parse_mode='HTML', reply_markup=choice_key_encryption_ru(chatid))
    elif message.data == "en_w_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nTo select an algorithm, just click on the cipher:\n\n''', parse_mode='HTML', reply_markup=choice_key_encryption_en(chatid))
    ##################################
    
    # 2 to 1 (Keys)
    ##################################
    elif message.data == "ru_b_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nДля выбора алгоритма нажмите на шифр:\n\n''', parse_mode='HTML', reply_markup=choice_key_encryption_ru(chatid))
    elif message.data == "en_b_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nTo select an algorithm, just click on the cipher:\n\n''', parse_mode='HTML', reply_markup=choice_key_encryption_en(chatid))
    ##################################
    
    
    # 3 to 2 (Word)
    ###################################
    elif message.data == "ru_ww_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери ключ:\n\n''''', parse_mode='HTML', reply_markup=choice_key_ru(chatid))
    elif message.data == "en_ww_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose key:\n\n''''', parse_mode='HTML', reply_markup=choice_key_en(chatid))
    ###################################
    
    # 3 to 2 (Keys)
    ###################################
    elif message.data == "ru_wk_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери длинну ключей:\n\n''''', parse_mode='HTML', reply_markup=choice_key_bytes_ru(chatid))
    elif message.data == "en_wk_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose length of keys:\n\n''''', parse_mode='HTML', reply_markup=choice_key_bytes_en(chatid))
    ###################################
    
    
    # 4 to 3 (Word)
    ##################################
    elif message.data == "cancel_ru_w":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_ru(chatid))
    elif message.data == "cancel_en_w":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_en(chatid))
    ##################################
    
    # 4 to 3 (Keys)
    ##################################
    elif message.data == "cancel_ru_k":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_ru(chatid))
    elif message.data == "cancel_en_k":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_en(chatid))
    ##################################
    
    
    
    ############# my 3 to 2 ################
    # (Text-Word)
    elif message.data == "w-key_text_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nTo select an algorithm, just click on the cipher:\n\n''', parse_mode='HTML', reply_markup=choice_encryption_text(chatid))
    ###############
    # (Text-Keys)
    elif message.data == "text_b_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nTo select an algorithm, just click on the cipher:\n\n''', parse_mode='HTML', reply_markup=choice_encryption_text(chatid))
    ########################################


    ############ my 4 to 3 #############
    # (Text-Word)
    elif message.data == "text_w_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose key:\n\n''''', parse_mode='HTML', reply_markup=choice_key_text(chatid))
    ###############
    # (Text-Keys)
    elif message.data == "text_k_return":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose length of keys:\n\n''', parse_mode='HTML', reply_markup=choice_key_bytes_text(chatid))
    ####################################


    ############ my 5 to 4 #############
    # (Text-Word)
    elif message.data == "cancel_text_w":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_w(chatid))
    ###############
    # (Text-Keys)
    elif message.data == "cancel_text_k":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_k(chatid))
    ####################################
    
    
    # my 3 to 2 (Document-Keys)
    ###################################
    elif message.data == "cancel_doc_k":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''''', parse_mode='HTML', reply_markup=choice_doc_k(chatid))
    
    # my 4 to 3 (Document-Word)
    ###################################
    elif message.data == "cancel_doc_w":
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''''', parse_mode='HTML', reply_markup=choice_doc_w(chatid))
    ###################################



    #################################################################################################################################################################################################################
    ##                                                                                              MY FIRST                                                                                                       ##
    ##                                                                                                                                                                                                             ##
    #################################################################################################################################################################################################################

    # My First step
    ################################
    elif message.data == 'type_Text':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nTo select an algorithm, just click on the cipher:\n\n''', parse_mode='HTML', reply_markup=choice_encryption_text(chatid))
    elif message.data == 'type_Document':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nTo select an algorithm, just click on the cipher:\n\n''', parse_mode='HTML', reply_markup=choice_encryption_doc(chatid))
    ####################################

    #################################################################################################################################################################################################################
    ##                                                                                              MY SECOND                                                                                                      ##
    ##                                                                                                                                                                                                             ##
    #################################################################################################################################################################################################################

    # My Second step for Text
    #####################################################
    elif message.data == 'choice_encryption_text_By Word':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose key:\n\n''', parse_mode='HTML', reply_markup=choice_key_text(chatid))
    elif message.data == 'choice_encryption_text_By Key':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose length of keys:\n\n''', parse_mode='HTML', reply_markup=choice_key_bytes_text(chatid))
    ####################################################

    # My Second step for Document 
    ####################################################
    elif message.data == 'choice_encryption_doc_By Word':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose key:\n\n''', parse_mode='HTML', reply_markup=choice_key_doc(chatid))
    elif message.data == 'choice_encryption_doc_By Key':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''', parse_mode='HTML', reply_markup=choice_doc_k(chatid))
    ###################################################

    #################################################################################################################################################################################################################
    ##                                                                                              MY THIRD                                                                                                       ##
    ##                                                                                                                                                                                                             ##
    #################################################################################################################################################################################################################

    # My Third step for text-Word
    ##################################
    elif message.data == 'text-key_Hi':
        key = 'Hi'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_w(chatid))
    #################
    elif message.data == 'text-key_helpful':
        key = 'helpful'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_w(chatid))
    #################
    elif message.data == 'text-key_blackboard':
        key = 'blackboard'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_w(chatid))
    #################
    elif message.data == 'text-key_mastery':
        key = 'mastery'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_w(chatid))
    #################
    elif message.data == 'text-key_Ascension Day':
        key = 'Ascension Day'
        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_w(chatid))
    #################
    elif message.data == 'text-key_at large':
        key = 'at large'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_w(chatid))
    #################
    elif message.data == 'text-key_be not worth-while':
        key = 'be not worth-while'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_w(chatid))
    #################
    elif message.data == 'text-key_fatality':
        key = 'fatality'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_w(chatid))
    ########################################

    # My Third step for text-Keys
    ##############################################
    elif message.data == 'my_choice_key_bytes_512':
        f = open('./keys/config', 'w')
        f.write('1')
        f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_k(chatid))
    #################
    elif message.data == 'my_choice_key_bytes_1024':
        f = open('./keys/config', 'w')
        f.write('2')
        f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_k(chatid))
    #################
    elif message.data == 'my_choice_key_bytes_2048':
        f = open('./keys/config', 'w')
        f.write('3')
        f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_k(chatid))
    #################
    elif message.data == 'my_choice_key_bytes_4096':
        f = open('./keys/config', 'w')
        f.write('4')
        f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_text_k(chatid))
    ###############################################


    # My Third step for doc-Word
    #################################
    elif message.data == 'doc-key_Hi':
        key = 'Hi'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''', parse_mode='HTML', reply_markup=choice_doc_w(chatid))
    #################
    elif message.data == 'doc-key_helpful':
        key = 'helpful'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''', parse_mode='HTML', reply_markup=choice_doc_w(chatid))
    #################
    elif message.data == 'doc-key_blackboard':
        key = 'blackboard'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''', parse_mode='HTML', reply_markup=choice_doc_w(chatid))
    #################
    elif message.data == 'doc-key_mastery':
        key = 'mastery'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''', parse_mode='HTML', reply_markup=choice_doc_w(chatid))
    #################
    elif message.data == 'doc-key_Ascension Day':
        key = 'Ascension Day'
        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''', parse_mode='HTML', reply_markup=choice_doc_w(chatid))
    #################
    elif message.data == 'doc-key_at large':
        key = 'at large'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''', parse_mode='HTML', reply_markup=choice_doc_w(chatid))
    #################
    elif message.data == 'doc-key_be not worth-while':
        key = 'be not worth-while'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''', parse_mode='HTML', reply_markup=choice_doc_w(chatid))
    #################
    elif message.data == 'doc-key_fatality':
        key = 'fatality'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nSelect size:\n\n''', parse_mode='HTML', reply_markup=choice_doc_w(chatid))
    #######################################

    # My Third step for doc-Keys
    ############################################
    elif message.data == 'k-doc_Big file(2.2gb)':
        f = open('./docs/big/b-video.mkv', 'rb')
        
        time_end = asymmetric_encrypt_file(f)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Time: {time_end}%', parse_mode='HTML', reply_markup=cancel_k(chatid))
    elif message.data == 'k-doc_Small file(0.6mb)':
        f = open('./docs/small/s-video.mov', 'rb')

        time_end = asymmetric_encrypt_file(f)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Time: {time_end}%', parse_mode='HTML', reply_markup=cancel_k(chatid))
    ##############################################

    #################################################################################################################################################################################################################
    ##                                                                                              MY FOURTH                                                                                                      ##
    ##                                                                                                                                                                                                             ##
    #################################################################################################################################################################################################################

    # My Third step for text-Word
    ####################################
    elif message.data == 'text_w_вообще':
        m = 'вообще'
            
        t_start = time.time()
            
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_text_w(chatid))
    #################
    elif message.data == 'text_w_вычет':
        m = 'вычет'

        t_start = time.time()
            
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_text_w(chatid))
    #################
    elif message.data == 'text_w_арена':
        m = 'арена'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_text_w(chatid))
    #################
    elif message.data == 'text_w_Андрей':
        m = 'Андрей'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_text_w(chatid))
    #################
    elif message.data == 'text_w_взаимодействие':
        m = 'взаимодействие'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_text_w(chatid))
    #################
    elif message.data == 'text_w_пиво':
        m = 'пиво'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_text_w(chatid))
    #################
    elif message.data == 'text_w_бегать взапуски':
        m = 'бегать взапуски'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_text_w(chatid))
    #################
    elif message.data == 'text_w_глубокомысленный':
        m = 'глубокомысленный'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_text_w(chatid))
    ##############################################


    # My Third step for text-Keys
    ####################################
    elif message.data == 'text_k_вообще':
        m = 'вообще'
            
        t_start = time.time()
            
        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)
        
        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_text_k(chatid))
    #################
    elif message.data == 'text_k_вычет':
        m = 'вычет'

        t_start = time.time()
            
        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)
        
        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_text_k(chatid))
    #################
    elif message.data == 'text_k_арена':
        m = 'арена'

        t_start = time.time()
        
        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)
        
        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_text_k(chatid))
    #################
    elif message.data == 'text_k_Андрей':
        m = 'Андрей'

        t_start = time.time()
        
        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)
        
        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_text_k(chatid))
    #################
    elif message.data == 'text_k_взаимодействие':
        m = 'взаимодействие'

        t_start = time.time()
        
        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_text_k(chatid))
    #################
    elif message.data == 'text_k_пиво':
        m = 'пиво'

        t_start = time.time()
        
        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)
        
        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_text_k(chatid))
    #################
    elif message.data == 'text_k_бегать взапуски':
        m = 'бегать взапуски'

        t_start = time.time()
        
        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)
        
        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_text_k(chatid))
    #################
    elif message.data == 'text_k_глубокомысленный':
        m = 'глубокомысленный'

        t_start = time.time()
        
        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)
        
        t_end = time.time() - t_start
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_text_k(chatid))
    ##############################################


    # My Fourth step for doc-Word
    ############################################
    elif message.data == 'w-doc_Big file(2.2gb)':
        f = open('./docs/big/b-video.mkv', 'rb')
        
        time_end = _symm_module.symmetric_encrypt_file(f)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Time: {time_end}%', parse_mode='HTML', reply_markup=cancel_w(chatid))
    elif message.data == 'w-doc_Small file(0.6mb)':
        f = open('./docs/small/s-video.mov', 'rb')
        
        time_end = _symm_module.symmetric_encrypt_file(f)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Time: {time_end}%', parse_mode='HTML', reply_markup=cancel_w(chatid))
    ##############################################


























    

    #################################################################################################################################################################################################################
    ##                                                                                               FIRST                                                                                                         ##
    ##                                                                                                                                                                                                             ##
    #################################################################################################################################################################################################################

    # First step
    ######################################################
    elif message.data == 'choice_key_encryption_ru_Словом':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери ключ:\n\n''', parse_mode='HTML', reply_markup=choice_key_ru(chatid))    
    elif message.data == 'choice_key_encryption_ru_Ключом':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери длинну ключей:\n\n''', parse_mode='HTML', reply_markup=choice_key_bytes_ru(chatid))
    ######################################################

    ######################################################
    elif message.data == 'choice_key_encryption_en_By Word':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose key:\n\n''', parse_mode='HTML', reply_markup=choice_key_en(chatid))
    elif message.data == 'choice_key_encryption_en_By Key':
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose length of keys:\n\n''', parse_mode='HTML', reply_markup=choice_key_bytes_en(chatid))
    ######################################################

    #################################################################################################################################################################################################################
    ##                                                                                               SECOND                                                                                                        ##
    ##                                                                                                                                                                                                             ##
    #################################################################################################################################################################################################################

    # Second step for Word
    ################################
    elif message.data == 'ru-key_Hi':
        key = 'Hi'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_ru(chatid))
    elif message.data == 'en-key_Hi':
        key = 'Hi'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_en(chatid))
    #################
    elif message.data == 'ru-key_helpful':
        key = 'helpful'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_ru(chatid))
    elif message.data == 'en-key_helpful':
        key = 'helpful'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_en(chatid))
    #################
    elif message.data == 'ru-key_blackboard':
        key = 'blackboard'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_ru(chatid))
    elif message.data == 'en-key_blackboard':
        key = 'blackboard'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_en(chatid))
    #################
    elif message.data == 'ru-key_mastery':
        key = 'mastery'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_ru(chatid))
    elif message.data == 'en-key_mastery':
        key = 'mastery'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_en(chatid))
    #################
    elif message.data == 'ru-key_Ascension Day':
        key = 'Ascension Day'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_ru(chatid))
    elif message.data == 'en-key_Ascension Day':
        key = 'Ascension Day'
        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_en(chatid))
    #################
    elif message.data == 'ru-key_at large':
        key = 'at large'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_ru(chatid))
    elif message.data == 'en-key_at large':
        key = 'at large'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_en(chatid))
    #################
    elif message.data == 'ru-key_be not worth-while':
        key = 'be not worth-while'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_ru(chatid))
    elif message.data == 'en-key_be not worth-while':
        key = 'be not worth-while'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_en(chatid))
    #################
    elif message.data == 'ru-key_fatality':
        key = 'fatality'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
                
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_ru(chatid))
    elif message.data == 'en-key_fatality':
        key = 'fatality'

        if os.path.exists("config"):
            os.remove("config")
            f = open('config', 'w')
            f.write(key)
            f.close()
        else:
            f = open('config', 'w')
            f.write(key)
            f.close()
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsw_en(chatid))
    ######################################


    # Second step for Key
    ##############################################
    elif message.data == 'ru_choice_key_bytes_512':
        (pub_key, priv_key) = rsa.newkeys(512)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_ru(chatid))
    elif message.data == 'en_choice_key_bytes_512':
        (pub_key, priv_key) = rsa.newkeys(512)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_en(chatid))
    #################
    elif message.data == 'ru_choice_key_bytes_1024':
        (pub_key, priv_key) = rsa.newkeys(1024)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_ru(chatid))
    elif message.data == 'en_choice_key_bytes_1024':
        (pub_key, priv_key) = rsa.newkeys(1024)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_en(chatid))
    #################
    elif message.data == 'ru_choice_key_bytes_2048':
        (pub_key, priv_key) = rsa.newkeys(2048)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_ru(chatid))
    elif message.data == 'en_choice_key_bytes_2048':
        (pub_key, priv_key) = rsa.newkeys(2048)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_en(chatid))
    #################
    elif message.data == 'ru_choice_key_bytes_4096':
        with open('public.pem', mode='rb') as publicfile:
            keydata = publicfile.read()
            pub_key = rsa.PublicKey.load_pkcs1(keydata)

        with open('private.pem', mode='rb') as privatefile:
            keydata = privatefile.read()
            priv_key = rsa.PrivateKey.load_pkcs1(keydata)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nВыбери слово для шифрования\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_ru(chatid))
    elif message.data == 'en_choice_key_bytes_4096':
        with open('public.pem', mode='rb') as publicfile:
            keydata = publicfile.read()
            pub_key = rsa.PublicKey.load_pkcs1(keydata)

        with open('private.pem', mode='rb') as privatefile:
            keydata = privatefile.read()
            priv_key = rsa.PrivateKey.load_pkcs1(keydata)

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text='''\n\nChoose word to encrypt:\n\n''', parse_mode='HTML', reply_markup=choice_key_wordsk_en(chatid))
    ###############################################

    #################################################################################################################################################################################################################
    ##                                                                                               THIRD                                                                                                         ##
    ##                                                                                                                                                                                                             ##
    #################################################################################################################################################################################################################

    # Third step for Word
    #######################################
    elif message.data == 'ru_wordsw_вообще':
        m = 'вообще'
            
        t_start = time.time()
            
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано словом: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nТекст: {decrypted_message}\n----------------------------------\nВремя: {t_end}%', reply_markup=cancel_ru_w(chatid))
    elif message.data == 'en_wordsw_вообще':
        m = 'вообще'
            
        t_start = time.time()
            
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_en_w(chatid))
    #################
    elif message.data == 'ru_wordsw_вычет':
        m = 'вычет'

        t_start = time.time()
            
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
            
        t_end = time.time() - t_start

        t_end *= 100
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано словом: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nТекст: {decrypted_message}\n----------------------------------\nВремя: {t_end}%', reply_markup=cancel_ru_w(chatid))
    elif message.data == 'en_wordsw_вычет':
        m = 'вычет'

        t_start = time.time()
            
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_en_w(chatid))
    #################
    elif message.data == 'ru_wordsw_арена':
        m = 'арена'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано словом: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nТекст: {decrypted_message}\n----------------------------------\nВремя: {t_end}%', reply_markup=cancel_ru_w(chatid))
    elif message.data == 'en_wordsw_арена':
        m = 'арена'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_en_w(chatid))
    #################
    elif message.data == 'ru_wordsw_Андрей':
        m = 'Андрей'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано словом: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nТекст: {decrypted_message}\n----------------------------------\nВремя: {t_end}%', reply_markup=cancel_ru_w(chatid))
    elif message.data == 'en_wordsw_Андрей':
        m = 'Андрей'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_en_w(chatid))
    #################
    elif message.data == 'ru_wordsw_взаимодействие':
        m = 'взаимодействие'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)

        t_end = time.time() - t_start

        t_end *= 100
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано словом: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nТекст: {decrypted_message}\n----------------------------------\nВремя: {t_end}%', reply_markup=cancel_ru_w(chatid))
    elif message.data == 'en_wordsw_взаимодействие':
        m = 'взаимодействие'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)

        t_end = time.time() - t_start

        t_end *= 100
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_en_w(chatid))
    #################
    elif message.data == 'ru_wordsw_пиво':
        m = 'пиво'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано словом: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nТекст: {decrypted_message}\n----------------------------------\nВремя: {t_end}%', reply_markup=cancel_ru_w(chatid))
    elif message.data == 'en_wordsw_пиво':
        m = 'пиво'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_en_w(chatid))
    #################
    elif message.data == 'ru_wordsw_бегать взапуски':
        m = 'бегать взапуски'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано словом: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nТекст: {decrypted_message}\n----------------------------------\nВремя: {t_end}%', reply_markup=cancel_ru_w(chatid))
    elif message.data == 'en_wordsw_бегать взапуски':
        m = 'бегать взапуски'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_en_w(chatid))
    #################
    elif message.data == 'ru_wordsw_глубокомысленный':
        m = 'глубокомысленный'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано словом: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nТекст: {decrypted_message}\n----------------------------------\nВремя: {t_end}%', reply_markup=cancel_ru_w(chatid))
    elif message.data == 'en_wordsw_глубокомысленный':
        m = 'глубокомысленный'

        t_start = time.time()
        
        # шифруем
        encrypted_message = _symm_module.symmetric_encrypt(m, verbose=False)
        encrypted_message_str = _symm_module.symmetric_encrypt(m, verbose=True)
        # расшифровываем
        decrypted_message = _symm_module.symmetric_decrypt(encrypted_message, verbose=False)
        decrypted_message_str = _symm_module.symmetric_decrypt(encrypted_message, verbose=True)
        
        t_end = time.time() - t_start

        t_end *= 100
            
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by word: {encrypted_message_str}\n----------------------------------\n{decrypted_message_str}\n----------------------------------\nText: {decrypted_message}\n----------------------------------\nTime: {t_end}%', reply_markup=cancel_en_w(chatid))
    #################################################


    # Third step for Key
    #######################################
    elif message.data == 'ru_wordsk_вообще':
        m = 'вообще'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано ключом: {encrypted_msg_asymm.hex()}\n----------------------------------\nТекст: {decrypted_msg_asymm.decode()}\nВремя: {t_end}%', reply_markup=cancel_ru_k(chatid))
    elif message.data == 'en_wordsk_вообще':
        m = 'вообще'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_en_k(chatid))
    #################
    elif message.data == 'ru_wordsk_вычет':
        m = 'вычет'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано ключом: {encrypted_msg_asymm.hex()}\n----------------------------------\nТекст: {decrypted_msg_asymm.decode()}\nВремя: {t_end}%', reply_markup=cancel_ru_k(chatid))
    elif message.data == 'en_wordsk_вычет':
        m = 'вычет'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_en_k(chatid))
    #################
    elif message.data == 'ru_wordsk_арена':
        m = 'арена'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано ключом: {encrypted_msg_asymm.hex()}\n----------------------------------\nТекст: {decrypted_msg_asymm.decode()}\nВремя: {t_end}%', reply_markup=cancel_ru_k(chatid))
    elif message.data == 'en_wordsk_арена':
        m = 'арена'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_en_k(chatid))
    #################
    elif message.data == 'ru_wordsk_Андрей':
        m = 'Андрей'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано ключом: {encrypted_msg_asymm.hex()}\n----------------------------------\nТекст: {decrypted_msg_asymm.decode()}\nВремя: {t_end}%', reply_markup=cancel_ru_k(chatid))
    elif message.data == 'en_wordsk_Андрей':
        m = 'Андрей'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start

        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_en_k(chatid))
    #################
    elif message.data == 'ru_wordsk_взаимодействие':
        m = 'взаимодействие'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)
        
        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано ключом: {encrypted_msg_asymm.hex()}\n----------------------------------\nТекст: {decrypted_msg_asymm.decode()}\nВремя: {t_end}%', reply_markup=cancel_ru_k(chatid))
    elif message.data == 'en_wordsk_взаимодействие':
        m = 'взаимодействие'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)
        
        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_en_k(chatid))
    #################
    elif message.data == 'ru_wordsk_пиво':
        m = 'пиво'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано ключом: {encrypted_msg_asymm.hex()}\n----------------------------------\nТекст: {decrypted_msg_asymm.decode()}\nВремя: {t_end}%', reply_markup=cancel_ru_k(chatid))
    elif message.data == 'en_wordsk_пиво':
        m = 'пиво'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_en_k(chatid))
    #################
    elif message.data == 'ru_wordsk_бегать взапуски':
        m = 'бегать взапуски'
        
        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано ключом: {encrypted_msg_asymm.hex()}\n----------------------------------\nТекст: {decrypted_msg_asymm.decode()}\nВремя: {t_end}%', reply_markup=cancel_ru_k(chatid))
    elif message.data == 'en_wordsk_бегать взапуски':
        m = 'бегать взапуски'
        
        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_en_k(chatid))
    #################
    elif message.data == 'ru_wordsk_глубокомысленный':
        m = 'глубокомысленный'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Зашифровано ключом: {encrypted_msg_asymm.hex()}\n----------------------------------\nТекст: {decrypted_msg_asymm.decode()}\nВремя: {t_end}%', reply_markup=cancel_ru_k(chatid))
    elif message.data == 'en_wordsk_глубокомысленный':
        m = 'глубокомысленный'

        t_start = time.time()

        # шифруем
        encrypted_msg_asymm = asymmetric_encrypt(m)
        # расшифровываем
        decrypted_msg_asymm = asymmetric_decrypt(encrypted_msg_asymm)

        t_end = time.time() - t_start
        
        bot.edit_message_text(chat_id=chatid, message_id=messageid, text=f'Encrypted by key: {encrypted_msg_asymm.hex()}\n----------------------------------\nText: {decrypted_msg_asymm.decode()}\nTime: {t_end}%', reply_markup=cancel_en_k(chatid))
    #################################################


###############  HELP  ################
@bot.message_handler(commands=['help'])
def command_help(m: Message):
    cid = m.chat.id
    lang = m.from_user.language_code
    if lang == 'ru':
        help_text = "Доступны следующие команды: \n"

        for key in commands_ru: # generate help text out of the commands dictionary defined at the top
            help_text += "/" + key + ": "
            help_text += commands_ru[key] + "\n"
        
        help_text += "Также ты можешь написать: How are you\nИ можешь написать: I love you\n"

        bot.send_message(cid, help_text)  # send the help page
    elif lang == 'en':
        help_text = "The following commands are available: \n"

        for key in commands_en: # generate help text out of the commands dictionary defined at the top
            help_text += "/" + key + ": "
            help_text += commands_en[key] + "\n"
        
        help_text += "Also you can type: How are you\nYou can type: I love you\n"

        bot.send_message(cid, help_text)  # send the help page
    

# filter on a specific message
#####################################################
@bot.message_handler(func=lambda m: m.text == "Шифр")
def command_text_ru(m: Message):
    chat_id = m.chat.id
    message_id = m.message_id
    msg = m.text

    if msg == 'Шифр':
        s = requests.Session()
        s.get('https://api.telegram.org/bot{0}/deletemessage?message_id={1}&chat_id={2}'.format(TOKEN, message_id, chat_id))

        text = 'Для выбора алгоритма нажмите на шифр:\n\n'
        bot.send_message(chat_id, text, parse_mode='HTML', reply_markup=choice_key_encryption_ru(chat_id))


# filter on a specific message
@bot.message_handler(func=lambda m: m.text == "Cipher")
def command_text_en(m: Message):
    chat_id = m.chat.id
    message_id = m.message_id
    msg = m.text

    if msg == 'Cipher':
        s = requests.Session()
        s.get('https://api.telegram.org/bot{0}/deletemessage?message_id={1}&chat_id={2}'.format(TOKEN, message_id, chat_id))

        if chat_id == 616255549:
            text = 'Select type:\n\n'
            bot.send_message(chat_id, text, parse_mode='HTML', reply_markup=choice_type(chat_id))
        else:
            text = 'To select an algorithm, just click on the cipher:\n\n'
            bot.send_message(chat_id, text, parse_mode='HTML', reply_markup=choice_key_encryption_en(chat_id))
#######################################################


""""""""""""""""""""""""""""""
""""""""""" For me """""""""""
# My First step
########################
def choice_type(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    d = ['Text',
         'Document']
    
    for i in d:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="type_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return to the main menu', callback_data="t_return"))
    
    return keyboard
########################


# My Second step for Text
###################################
def choice_encryption_text(chat_id):
    keyboard = types.InlineKeyboardMarkup()

    ch = ['By Word', 
          'By Key']
    
    for i in ch:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="choice_encryption_text_{0}".format(i)))
    
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="_text_return"))

    return keyboard
###################################

# My Second step for Document
##################################
def choice_encryption_doc(chat_id):
    keyboard = types.InlineKeyboardMarkup()

    ch = ['By Word', 
          'By Key']
    
    for i in ch:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="choice_encryption_doc_{0}".format(i)))
    
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="_doc_return"))

    return keyboard
##################################


# My Third step for text-Word
###########################
def choice_key_text(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    w = ['Hi', 
         'helpful', 
         'blackboard', 
         'mastery',
         'Ascension Day', 
         'at large', 
         'be not worth-while', 
         'fatality']
    
    for i in w:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="text-key_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="w-key_text_return"))
    
    return keyboard
###########################

# My Third step for text-Keys
##################################
def choice_key_bytes_text(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    b = ['512', 
         '1024', 
         '2048', 
         '4096']
    
    for i in b:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="my_choice_key_bytes_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="text_b_return"))
    
    return keyboard
##################################


# My Third step for doc-Word
###########################
def choice_key_doc(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    w = ['Hi', 
         'helpful', 
         'blackboard', 
         'mastery',
         'Ascension Day', 
         'at large', 
         'be not worth-while', 
         'fatality']
    
    for i in w:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="doc-key_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="w-key_doc_return"))
    
    return keyboard
###########################

# My Third step for doc-Keys
#########################
def choice_doc_k(chat_id):
    keyboard = types.InlineKeyboardMarkup()

    ch = ['Big file(2.2gb)', 
          'Small file(0.6mb)']
    
    for i in ch:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="k-doc_{0}".format(i)))
    
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="k_doc_return"))

    return keyboard
#########################


# My Fourth step for text-Word
##########################
def choice_text_w(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    w = ['вообще', 
         'вычет', 
         'арена', 
         'Андрей',
         'взаимодействие', 
         'пиво', 
         'бегать взапуски', 
         'глубокомысленный']
    
    for i in w:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="text_w_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="text_w_return"))
    
    return keyboard
##########################


# My Fourth step for text-Keys
##########################
def choice_text_k(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    w = ['вообще', 
          'вычет', 
          'арена', 
          'Андрей',
          'взаимодействие', 
          'пиво', 
          'бегать взапуски', 
          'глубокомысленный']
    
    for i in w:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="text_k_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="text_k_return"))
    
    return keyboard
##########################


# My Fourth step for doc-Word
#########################
def choice_doc_w(chat_id):
    keyboard = types.InlineKeyboardMarkup()

    ch = ['Big file(2.2gb)', 
          'Small file(0.6mb)']
    
    for i in ch:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="w-doc_{0}".format(i)))
    
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="w_doc_return"))

    return keyboard
#########################


# text-Word
#####################
def cancel_text_w(chat_id):
    keyboard = types.InlineKeyboardMarkup()
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="cancel_text_w"))
    
    return keyboard
#####################

# text-Keys
#####################
def cancel_text_k(chat_id):
    keyboard = types.InlineKeyboardMarkup()
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="cancel_text_k"))
    
    return keyboard
#####################


# doc-Word
#####################
def cancel_w(chat_id):
    keyboard = types.InlineKeyboardMarkup()
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="cancel_doc_w"))
    
    return keyboard
#####################

# doc-Keys
#####################
def cancel_k(chat_id):
    keyboard = types.InlineKeyboardMarkup()
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="cancel_doc_k"))
    
    return keyboard
#####################
""""""""""""""""""""""""""""""


# First step
#####################################
def choice_key_encryption_ru(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    ch = ['Словом', 
          'Ключом']
    
    for i in ch:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="choice_key_encryption_ru_{0}".format(i)))
    
    keyboard.add(types.InlineKeyboardButton(text='<-- Вернуться в главное меню', callback_data="ru_ch_return"))
    
    return keyboard

def choice_key_encryption_en(chat_id):
    keyboard = types.InlineKeyboardMarkup()

    ch = ['By Word', 
          'By Key']
    
    for i in ch:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="choice_key_encryption_en_{0}".format(i)))
    
    keyboard.add(types.InlineKeyboardButton(text='<-- Return to the main menu', callback_data="en_ch_return"))

    return keyboard
#####################################

# Second step for Word
##########################
def choice_key_ru(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    w = ['Hi', 
         'helpful', 
         'blackboard', 
         'mastery',
         'Ascension Day', 
         'at large', 
         'be not worth-while', 
         'fatality']
    
    for i in w:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="ru-key_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Назад', callback_data="ru_w_return"))
    
    return keyboard

def choice_key_en(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    w = ['Hi', 
         'helpful', 
         'blackboard', 
         'mastery',
         'Ascension Day', 
         'at large', 
         'be not worth-while', 
         'fatality']
    
    for i in w:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="en-key_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="en_w_return"))
    
    return keyboard
##########################

# Second step for Keys
################################
def choice_key_bytes_ru(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    b = ['512', 
         '1024', 
         '2048', 
         '4096']
    
    for i in b:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="ru_choice_key_bytes_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Назад', callback_data="ru_b_return"))
    
    return keyboard

def choice_key_bytes_en(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    b = ['512', 
         '1024', 
         '2048', 
         '4096']
    
    for i in b:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="en_choice_key_bytes_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="en_b_return"))
    
    return keyboard
################################

# Third step for Word
#################################
def choice_key_wordsw_ru(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    ww = ['вообще', 
         'вычет', 
         'арена', 
         'Андрей',
         'взаимодействие', 
         'пиво', 
         'бегать взапуски', 
         'глубокомысленный']
    
    for i in ww:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="ru_wordsw_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Назад', callback_data="ru_ww_return"))
    
    return keyboard

def choice_key_wordsw_en(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    ww = ['вообще', 
         'вычет', 
         'арена', 
         'Андрей',
         'взаимодействие', 
         'пиво', 
         'бегать взапуски', 
         'глубокомысленный']
    
    for i in ww:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="en_wordsw_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="en_ww_return"))
    
    return keyboard
#################################

# Third step for Keys
#################################
def choice_key_wordsk_ru(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    wk = ['вообще', 
          'вычет', 
          'арена', 
          'Андрей',
          'взаимодействие', 
          'пиво', 
          'бегать взапуски', 
          'глубокомысленный']
    
    for i in wk:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="ru_wordsk_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Назад', callback_data="ru_wk_return"))
    
    return keyboard

def choice_key_wordsk_en(chat_id):
    keyboard = types.InlineKeyboardMarkup()
    
    wk = ['вообще', 
          'вычет', 
          'арена', 
          'Андрей',
          'взаимодействие', 
          'пиво', 
          'бегать взапуски', 
          'глубокомысленный']
    
    for i in wk:
        keyboard.add(types.InlineKeyboardButton(text=i, callback_data="en_wordsk_{0}".format(i)))
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="en_wk_return"))
    
    return keyboard
#################################

# Cancel button for Word
########################
def cancel_ru_w(chat_id):
    keyboard = types.InlineKeyboardMarkup()
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Назад', callback_data="cancel_ru_w"))
    
    return keyboard

def cancel_en_w(chat_id):
    keyboard = types.InlineKeyboardMarkup()
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="cancel_en_w"))
    
    return keyboard
########################

# Cancel button for Keys
########################
def cancel_ru_k(chat_id):
    keyboard = types.InlineKeyboardMarkup()
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Назад', callback_data="cancel_ru_k"))
    
    return keyboard

def cancel_en_k(chat_id):
    keyboard = types.InlineKeyboardMarkup()
        
    keyboard.add(types.InlineKeyboardButton(text='<-- Return', callback_data="cancel_en_k"))
    
    return keyboard
########################


########################################
@bot.message_handler(commands=['debug'])
def command_debug(m):
    chat_id = m.chat.id
    lang = m.from_user.language_code
    
    if chat_id == 616255549:
        f = open('./docs/big/b-video.mkv', 'rb')
        
        time_end = asymmetric_encrypt_file(f)

        p = '%.2f' % (time_end) + '%'

        if lang == 'ru':
            bot.send_message(chat_id, f'Время: {p}')
        elif lang == 'en':
            bot.send_message(chat_id, f'Time: {p}')
########################################
        

# filter on a specific message
###########################################################
@bot.message_handler(func=lambda m: m.text == "I love you")
def command_text_hi(m: Message):
    if m.chat.id == 616255549:
        bot.send_message(m.chat.id, "I love you too, creator!")
    else:
        bot.send_message(m.chat.id, "I love you too!")


@bot.message_handler(func=lambda m: m.text == "How are you")
def command_text_hi(m: Message):
    import random
    h = ['Good!', 
         'Fine.',
         'Bored',
         'Grr–. Grr–.',
         'Not good.',
         'Not so good.']
    e = ['😀', '😃', '😄', '😁', '😆', '😅', '😎', '😗', '😚', '😖', '🙁', '😍', '🤮', '😓', '😬', '🤕', '💩', '👿', '💩', '👻', '🤔', '😐', '😱', '😢']
    
    bot.send_message(m.chat.id, random.choice(h) + '\n' + random.choice(e))
###########################################################

if __name__ == '__main__':
    bot.polling(none_stop=True, interval=0, timeout=3)
